package com.citi.training.stocks.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.stocks.model.Stock;
import com.citi.training.stocks.repo.StockDao;

@RestController
@RequestMapping("/stocks")
public class StockController {

	private static final Logger LOG = LoggerFactory.getLogger(StockController.class);
	
	@Autowired
	StockDao stockRepository;
	
    @RequestMapping(method=RequestMethod.GET)
    public Iterable<Stock> findAll(){
    	LOG.info("HTTP GET to findAll()");
    	LOG.debug("This is a much verbose debugging message!");
    	return stockRepository.findAll();
    }
    
    @RequestMapping(method=RequestMethod.POST)
    public Stock create(@RequestBody Stock stock) {
    	stockRepository.save(stock);
    	return stock;
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public Stock findById(@PathVariable long id) {
    	return stockRepository.findById(id).get();
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public void deleteById(@PathVariable long id) {
    	stockRepository.deleteById(id);
    }
    
}
